﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Interface
{
    interface IStudent
    {
        string Name { get; set; }
        int Age { get; set; }

        void Study();

        void GoToSchool();
    }
}
