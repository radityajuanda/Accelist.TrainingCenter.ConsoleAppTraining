﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Services
{
    public class Greeting
    {
        public void Hello() {
            Console.WriteLine("Type your name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Hello, " + name);
        }
    }
}
