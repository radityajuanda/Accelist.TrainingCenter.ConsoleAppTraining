﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Services
{
    public class ReverseString
    {
        public void Reverse()
        {
            Console.WriteLine("Input string: ");
            string str = Console.ReadLine();
            char[] charStr = str.ToCharArray();

            Array.Reverse(charStr);

            Console.WriteLine("Reversed string: " + new string(charStr));
        }
    }
}
