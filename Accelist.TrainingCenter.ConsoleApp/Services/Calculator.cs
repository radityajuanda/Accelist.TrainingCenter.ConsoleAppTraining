﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Services
{
    class Calculator
    {
        public void Menu()
        {
            Console.WriteLine("Input first number: ");
            decimal num1 = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Input second number: ");
            decimal num2 = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Input operator: ");
            string op = Console.ReadLine();

            if (op == "+")
                Console.WriteLine("The result is " + Add(num1, num2));
            else if (op == "-")
                Console.WriteLine("The result is " + Subtract(num1, num2));
            else if (op == "*")
                Console.WriteLine("The result is " + Multiply(num1, num2));
            else if (op == "/")
                Console.WriteLine("The result is " + Divide(num1, num2));
            else
                Console.WriteLine("Unidentified operator!");
        }

        public decimal Add(decimal num1, decimal num2)
        {
            return num1 + num2;
        }

        public decimal Subtract(decimal num1, decimal num2)
        {
            return num1 - num2;
        }

        public decimal Multiply(decimal num1, decimal num2)
        {
            return num1 * num2;
        }

        public decimal Divide(decimal num1, decimal num2)
        {
            return num1 / num2;
        }
    }
}
