﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Model
{
    public enum Position
    {
        Chief, Manager, Staff
    }

    public class Employee
    {
        public Position Position { get; set; }

        public int EmployeeID { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }


        // Propfull, dipake klo mau ngatur value(misal fixed value) ato kasih logic
        //private int myVar;

        //public int MyProperty
        //{
        //    get { return myVar; }
        //    set { myVar = value; }
        //}

        public Tuple<int, string> EmployeeTuple { get; set; }

        public Tuple<int, string> FillEmployeeTuple()
        {
            var employeeTuple = new Tuple<int, string>(170, "Hindra");
            return employeeTuple;
        }

        public void GetTuple()
        {
            var tuple = FillEmployeeTuple();
            Console.WriteLine(tuple.Item1 + " " + tuple.Item2);
        }


        public Employee SetEmployeeProfile()
        {
            Employee employee = new Employee();
            employee.EmployeeID = 1;
            employee.Name = "Jack";
            employee.Age = 20;
            employee.Position = Position.Chief;

            return employee;
        }

        public void CreateAnonymousType()
        {
            var anon = new { Food = "Ramen", Price = 10000 };
            Console.WriteLine(anon.Food);
            Console.WriteLine(anon.Price);
        }

        public dynamic GetDynamicData(int number)
        {
            if (number == 1)
                return "You're number 1";
            else if (number == 2)
                return 2 * number;
            else
                return "tes";
        }
    }

    public struct StructEmployee
    {
        public int EmployeeID { get; set; }

        public string Name { get; set; }

        private int age;

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public StructEmployee SetEmployeeProfile()
        {
            StructEmployee structEmployee = new StructEmployee();
            structEmployee.EmployeeID = 1;
            structEmployee.Name = "Jack";
            structEmployee.Age = 20;

            return structEmployee;
        }
    }
}
