﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Model
{
    public abstract class Creature
    {
        public int Age { get; set; }

        public virtual void Breathe()
        {
            Console.WriteLine("Breathe");
        }
    }
}
