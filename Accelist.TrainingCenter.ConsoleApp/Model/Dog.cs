﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Model
{
    class Dog : Mammal
    {
        public string Name { get; set; }

        public void Walking()
        {
            // Do walking
            Console.WriteLine("I'm walking");
        }

        public void Fetch()
        {
            // Do fetch
            Breathe();
            Console.WriteLine("I'm fetching the ball");
        }

        public override void Breathe()
        {
            Console.WriteLine("B");
        }
    }
}
