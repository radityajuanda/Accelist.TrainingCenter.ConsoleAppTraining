﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Model
{
    class Util
    {
        public int? nullableNumber = null;
        Nullable<Int32> nullableInt32 = null;
        public float? nullableFloat = null;
        public string nullString = null;

        public Employee NullEmployee = null;


        public void RunCompanny()
        {
            NullEmployee = new Employee();
            NullEmployee.Age = 21;
            var employeeAge = NullEmployee.Age;
            Console.WriteLine(employeeAge);
        }

        public void CoalescingOperator()
        {
            int? x = 0;
            int? y = 1;
            int notNull = 10;
            int z = x ?? y ?? notNull;
            Console.WriteLine(z);
        }

        public void ListVsArray()
        {
            var employees = new List<Employee>();
            var employeesArray = new Employee[10];

            employees.Add(new Employee{
               Name = "John"
            });

            employees.Add(new Employee
            {
                Name = "Doe"
            });

            foreach (var employee in employees)
            {
                Console.WriteLine(employee.Name);
            }
        }

        public void UsingAsKeyword()
        {
            var stringNumber = "123";
            var i = stringNumber as string;

            // Boxing, untuk dicek tipe datanya
            object test = new Employee();

            var dog = new Dog();
            var mammal = test as Mammal;

            // Is balikin true false, As balikin value ato null
            if(test is string)
                Console.WriteLine("Is string");
            else
                Console.WriteLine("Is not string");
        }
    }
}
