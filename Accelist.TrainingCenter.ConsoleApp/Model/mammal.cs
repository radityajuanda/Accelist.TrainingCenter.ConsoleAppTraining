﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp.Model
{
    public abstract class Mammal : Creature
    {
        public void breeding()
        {
            // Do breeding
            Console.WriteLine("Don't disturb me");
        }

        public void nursing()
        {
            // Do nursing
            Console.WriteLine("I'm nursing");
        }
    }
}
