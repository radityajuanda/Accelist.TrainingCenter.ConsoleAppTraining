﻿using Accelist.TrainingCenter.ConsoleApp.Model;
using Accelist.TrainingCenter.ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingCenter.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Menu:");
            //Console.WriteLine("1. Greetings");
            //Console.WriteLine("2. Calculator");
            //Console.WriteLine("3. Reverse String");

            //string menuInput = Console.ReadLine();
            //if (menuInput == "1")
            //    new Greeting().Hello();
            //if (menuInput == "2")
            //    new Calculator().Menu();
            //if (menuInput == "3")
            //    new ReverseString().Reverse();

            //CheckEmployee

            //int valRef = 10;
            //MethodRef(ref valRef);
            //Console.WriteLine(valRef);

            //int valOut = 20;
            //MethodOut(out valOut);
            //Console.WriteLine(valOut);

            //Employee employee = new Employee();
            //employee.GetTuple();

            //employee.CreateAnonymousType();
            //employee.GetDynamicData(1);

            Util util = new Util();
            //util.RunCompanny();
            //util.ListVsArray();
            util.UsingAsKeyword();

            //Dog dog = new Dog();
            //dog.Breathe();



            Console.Read();
        }

        // Variable yg jadi parameter berubah jg valuenya
        public static void MethodRef(ref int i)
        {
            i = i + 44;
        }

        // Sama kayak ref cuma parameternya blum di assign valuenya(assign manual)
        public static void MethodOut(out int i)
        {
            i = 10 + 44;
        }

        public static void CheckEmployee()
        {
            Employee employee = new Employee();
            Employee employee1 = new Employee()
            {
                Age = 21,
                EmployeeID = 1,
                Name = "Jack"
            };

            List<Employee> employees = new List<Employee>();
            employees.Add(new Employee {Name = "Raditya", Age = 20, EmployeeID = 2});

            // Cara bikin list laen
            //List<Employee> employees = new List<Employee>
            //{
            //    new Employee {Age = 30, Name = "John" },
            //    new Employee {Age = 25, Name = "Doe" }
            //};

            Console.WriteLine(employee1.Age);
            Console.WriteLine(employee1.EmployeeID);
            Console.WriteLine(employee1.Name);
        }
    }
}
